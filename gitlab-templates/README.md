# Our GitLab CI/CD Pipelines

This folder has templates that can be included in your project's `.gitlab-ci.yml` file by using their full file path
in this repository and the [`include:` keyword](https://invent.kde.org/help/ci/yaml/index.md#includeproject). For example:

```yaml
include:
  - project: sysadmin/ci-utilities
    file:
      - /gitlab-templates/freebsd-qt6.yml
      - /gitlab-templates/linux-qt6.yml
```

For further details about our pipeline infrastructure, see [Continuous Integration System](https://community.kde.org/Infrastructure/Continuous_Integration_System).

## CI Pipelines

### General
* json-validation.yml - Validates JSON files in the project
* reuse-lint.yml - Validates [REUSE compliance](https://community.kde.org/Policies/Licensing_Policy) of the project

### Linux
* linux.yml, linux-qt6.yml - Builds and runs unit tests for the project with Qt 5 and Qt 6 respectively
* linux-static.yml, linux-qt6-static.yml - Creates statically linked builds with Qt 5 and Qt 6 respectively

### FreeBSD
* freebsd.yml, freebsd-qt6.yml - Builds and runs unit tests for the project with Qt 5 and Qt 6 respectively

### Windows
* windows.yml, windows-qt6.yml - Builds and runs unit tests for the project with Qt 5 and Qt 6 respectively
* windows-static.yml - Creates statically linked builds with Qt 5

### Android
* android.yml, android-qt6.yml - Cross-builds the project with Qt 5 and Qt 6 respectively

## CD Pipelines

### Linux
* craft-appimage.yml - Builds an AppImage for the project
* flatpak.yml - Builds a Flatpak for the project

### Windows
* craft-windows-mingw64.yml - Builds the project with MingW64 and creates some installers
* craft-windows-x86-64.yml - Builds the project with MSVC and creates some installers
* craft-windows-base.yml - Common code of the Windows pipelines

### macOS
* craft-macos-arm64.yml, craft-macos-x86-64.yml - Builds the project for ARM64 and x86
* craft-macos-base.yml - Common code of the macOS pipelines

### Android
* craft-android-apks.yml, craft-android-qt6-apks.yml -
Builds signed APKs for the project for ARM32, ARM64, and Intel 64-bit with Qt 5 and Qt 6 respectively.
Publishes APKs in KDE's F-Droid repositories.
* craft-android-appbundle.yml - Repackages the platform-specific APKs (Qt 5) in one [Android App Bundle](https://developer.android.com/guide/app-bundle) (AAB)
* craft-android-qt5-googleplay-apks.yml - Submits APKs to Google Play.

## Website Pipelines
* website-hugo.yml
* website-sphinx-app-docs.yml
* website-base.yml - Common code of the website pipelines

## Details

### Craft Pipelines

All pipelines using Craft assume that the name of the project's Craft blueprint matches
the name of the project's repository ([CI_PROJECT_NAME](https://invent.kde.org/help/ci/variables/predefined_variables.md)).

The pipelines can be customized with a `.craft.ini` file in the project's root
folder to override the defaults of Craft and the Craft blueprints. Some examples which
illustrate the syntax:

```
[BlueprintSettings]
kde/applications/kate.packageAppx = True
kde/applications/konsole.version = master
```
This `.craft.ini` could be used by Kate. Setting the `packageAppx` option to `True` for Kate
enables the creation of APPX packages for Kate. And setting `version` of `konsole` to `master`
tells Craft that the `master` version of Konsole should be used for Kate.

```
[BlueprintSettings]
kde/pim.version = release/23.08
kde/libs.version = master
kde/plasma-mobile.version = master
```
This `.craft.ini` is/was used by Itinerary. We need the stable `release/23.08` versions
of the PIM libraries and the `master` versions of the `kde/libs` dependencies and the
`kde/plasma-mobile` dependencies.

```
[android-arm-clang-BlueprintSettings]
libs/qt5/qtsvg.featureArguments = -feature-qgraphicssvgitem

[android-arm64-clang-BlueprintSettings]
libs/qt5/qtsvg.featureArguments = -feature-qgraphicssvgitem

[android-x86_64-clang-BlueprintSettings]
libs/qt5/qtsvg.featureArguments = -feature-qgraphicssvgitem
```
This `.craft.ini` shows how to specify special settings for the Android builds.

### Android CD Pipelines

The CD pipelines for Android build signed or unsigned APKs and AABs and publish
them in our F-Droid repositories and on Google Play. Signed APKs are only created
for a few branches, typically the latest release branch for stable builds and the
master branch for nightly builds. Likewise, publication happens only for certain
branches.

To enable signing and publication for your project you need to edit the [project
settings](signing/README.md) of the corresponding CI notary services.
