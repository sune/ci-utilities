#!/usr/bin/python3
# Version in sysadmin/ci-utilities should be single source of truth
# SPDX-FileCopyrightText: 2023 Alexander Lohnau <alexander.lohnau@gmx.de>
# SPDX-License-Identifier: BSD-2-Clause

import argparse
import os
import subprocess
import yaml

parser = argparse.ArgumentParser(description='Check JSON schema for files in repository')
parser.add_argument('--schemafile', required=True, help='Path to the schema file')
parser.add_argument('--check-all', default=False, action='store_true', help='If all files should be checked or only the changed files')
parser.add_argument('--verbose', default=False, action='store_true')
args = parser.parse_args()

def get_changed_files() -> list[str]:
    result = subprocess.run(['git', 'diff', '--cached', '--name-only'], capture_output=True, text=True)
    return [file for file in result.stdout.splitlines() if file.endswith('.json')]

def get_all_files() -> list[str]:
    files = []
    for root, _, filenames in os.walk('.'):
        for filename in filenames:
            if filename.endswith('.json'):
                files.append(os.path.join(root, filename))
    return files

def filter_excluded_included_json_files(files: list[str]) -> list[str]:
    config_file = '.kde-ci.yml'
    # Check if the file exists
    if os.path.exists(config_file):
        with open(config_file, 'r') as file:
            config = yaml.safe_load(file)
    else:
        if args.verbose:
            print(f'{config_file} does not exist in current directory')
        config = {}
    # Extract excluded files, used for tests that intentionally have broken files
    excluded_files = ['compile_commands.json', 'ci-utilities']
    if 'Options' in config and 'json-validate-ignore' in config['Options']:
        excluded_files += config['Options']['json-validate-ignore']

    # Find JSON files
    filtered_files = []
    for file_path in files:
        if not any(excluded_file in file_path for excluded_file in excluded_files):
            filtered_files.append(file_path)
    
    # "include" overrides the "ignore" files, so if a file is both included and excluded, it will be included
    if 'Options' in config and 'json-validate-include' in config['Options']:
        filtered_files += config['Options']['json-validate-include']

    return filtered_files

if args.check_all:
    files = get_all_files()
else:
    files = get_changed_files()
files = filter_excluded_included_json_files(files)

if files:
    files_option = ' '.join(files)
    if args.verbose:
        print(f"Validating {files_option} with {args.schemafile}")
    result = subprocess.run(['check-jsonschema', *files, '--schemafile', args.schemafile])
    # Fail the pipeline if command failed
    if result.returncode != 0:
        exit(1)

